import Phaser from 'phaser';
import config from 'visual-config-exposer';

let destroyCounter = 0;

let rock, player, ground;
let smallRock1, smallRock2;

class Game extends Phaser.Scene {
  constructor() {
    super('playGame');
    this.GAME_WIDTH = 740;
    this.GAME_HEIGHT = 900;
  }

  create() {
    const background = this.add.image(0, 0, 'bg');
    background.setOrigin(0, 0);
    background.setScale(1, 0.95);

    ground = this.physics.add.image(
      this.GAME_WIDTH / 2,
      this.GAME_HEIGHT + 40,
      'ground',
      null,
      {
        isStatic: 'true',
        restitution: 1,
      }
    );
    ground.setOrigin(0.5, 1.25);
    ground.setSize(this.GAME_WIDTH * 2, 50);
    ground.setOffset(250, 230);
    ground.setScale(0.6);
    console.log(ground);
    ground.height = 10;
    ground.body.allowGravity = false;
    ground.setImmovable();

    this.createRock();

    this.physics.add.collider(ground, rock);

    this.createPlayer();

    this.gameTimer = this.time.addEvent({
      delay: 50,
      callback: function () {
        const beam = this.physics.add.image(player.x, player.y, 'beam');
        beam.setScale(0.2);
        beam.angle = -90;
        beam.setVelocityY(-1000);
        this.physics.add.overlap(
          rock,
          beam,
          function () {
            beam.destroy();
            rock.health--;
          },
          null,
          this
        );
        if (smallRock1) {
          this.physics.add.overlap(
            smallRock1,
            beam,
            function () {
              beam.destroy();
              smallRock1.health--;
              console.log(`smallrock health 1 ${smallRock1.health}`);
            },
            null,
            this
          );
        }
        if (smallRock2) {
          this.physics.add.overlap(
            smallRock2,
            beam,
            function () {
              beam.destroy();
              smallRock2.health--;
              console.log(`smallrock health 2 ${smallRock2.health}`);
            },
            null,
            this
          );
        }
        setTimeout(function () {
          if (beam) {
            beam.destroy();
          }
        }, 800);
      },
      callbackScope: this,
      loop: true,
    });
  }

  update() {
    if (rock.health === 0 && destroyCounter === 0) {
      this.createSubRocks(rock.body.x, rock.body.y);
      rock.destroy();
      destroyCounter++;
      console.log(destroyCounter);
    }

    if (
      smallRock1 &&
      smallRock1.health <= 0 &&
      (destroyCounter === 1 || destroyCounter === 2)
    ) {
      smallRock1.destroy();
      destroyCounter++;
      console.log(destroyCounter);
    }

    if (
      smallRock2 &&
      smallRock2.health <= 0 &&
      (destroyCounter === 2 || destroyCounter === 3)
    ) {
      smallRock2.destroy();
      destroyCounter++;
      console.log(destroyCounter);
    }
  }

  createRock() {
    rock = this.physics.add.image(
      this.GAME_WIDTH / 2,
      this.GAME_HEIGHT / 10,
      'bigRock'
    );
    rock.setCircle(120);
    rock.health = 5;
    rock.setScale(0.4);
    rock.setBounce(1);
    rock.setVelocity(-500, 150);
    rock.setCollideWorldBounds();
    rock.body.allowGravity = true;
  }

  createPlayer() {
    player = this.physics.add.image(
      this.GAME_WIDTH / 2,
      this.GAME_HEIGHT - 100,
      'player'
    );
    player.setScale(0.2);
    player.setBounce(1);
    player.setImmovable();
    player.body.allowGravity = false;
    player.setCollideWorldBounds();
    player.setInteractive();
    player.health = 10;

    this.input.setDraggable(player);
    this.input.on('drag', function (pointer, gameObject, dragX, dragY) {
      gameObject.x = dragX;
    });

    this.physics.add.collider(
      player,
      rock,
      function () {
        player.health--;
      },
      null,
      this
    );
  }

  createSubRocks(x, y) {
    smallRock1 = this.physics.add.image(x, y, 'smallRock');
    smallRock1.health = 20;
    smallRock1.setScale(0.1);
    smallRock1.setBounce(1);
    smallRock1.setVelocity(-500, 150);
    smallRock1.setCollideWorldBounds();
    smallRock1.body.allowGravity = true;
    smallRock1.setCircle(220);

    this.physics.add.collider(ground, smallRock1);

    this.physics.add.collider(
      player,
      smallRock1,
      function () {
        player.health--;
        console.log(player.health);
      },
      null,
      this
    );

    smallRock2 = this.physics.add.image(x, y, 'smallRock');
    smallRock2.health = 20;
    smallRock2.setScale(0.1);
    smallRock2.setBounce(1);
    smallRock2.setVelocity(500, -150);
    smallRock2.setCollideWorldBounds();
    smallRock2.body.allowGravity = true;
    smallRock2.setCircle(220);

    this.physics.add.collider(ground, smallRock2);

    this.physics.add.collider(
      player,
      smallRock2,
      function () {
        player.health--;
        console.log(player.health);
      },
      null,
      this
    );
  }
}

export default Game;
